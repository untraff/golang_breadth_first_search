package main

import (
	"fmt";
	"container/list"
)

type Node struct {
	x, y int
	parent *Node
}

type simpleNode struct {
	x, y int
}

func getNeighbors(labyMap map[int]map[int]int, curNode Node, passNodes map[simpleNode]int8) ([]Node){
	neighbors := []Node{}
	nbChoice :=  [4]simpleNode{
		simpleNode{0, 1},
		simpleNode{0, -1},
		simpleNode{1, 0},
		simpleNode{-1, 0},
	}
	for i := range(nbChoice){
		newX := curNode.x + nbChoice[i].x
		newY := curNode.y + nbChoice[i].y
		if isWay, ok := labyMap[newY][newX]; ok{
			if _, checked := passNodes[simpleNode{newX, newY}]; !checked && isWay == 0{
				neighbors = append(neighbors, Node{newX, newY, &curNode})
			}
		}
	}
	return neighbors
}

// TODO: создать мапу из файла
var labyMap = map[int]map[int]int {
	0: map[int]int{0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 1, 9: 1},
	1: map[int]int{0: 0, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 0, 8: 1, 9: 1},
	2: map[int]int{0: 0, 1: 0, 2: 1, 3: 0, 4: 0, 5: 0, 6: 1, 7: 0, 8: 1, 9: 1},
	3: map[int]int{0: 1, 1: 0, 2: 1, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 1, 9: 1},
	4: map[int]int{0: 1, 1: 0, 2: 1, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 1, 9: 1},
	5: map[int]int{0: 1, 1: 0, 2: 1, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 1, 9: 1},
	6: map[int]int{0: 1, 1: 0, 2: 1, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 1, 9: 1},
	7: map[int]int{0: 1, 1: 0, 2: 1, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 1, 9: 1},
	8: map[int]int{0: 1, 1: 0, 2: 1, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 1, 9: 1},
	9: map[int]int{0: 1, 1: 0, 2: 0, 3: 0, 4: 1, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0},
}

func main() {
	startNode := Node{0, 0, nil}
	endNode := Node{9, 9, nil}  // TODO: находить ноду
	var passNodes map[simpleNode]int8
	passNodes = make(map[simpleNode]int8)
	queue := list.New()
	queue.PushBack(startNode)

MyLoop:  // TODO: как лучше выходить из цикла
	for queue.Len() > 0 {

		realNode := queue.Front()
		curNode := realNode.Value.(Node)
		passNodes[simpleNode{curNode.x, curNode.y}] = 1
		queue.Remove(realNode)

		if curNode.x == endNode.x && curNode.y == endNode.y{
			var wayBackNode *Node
			wayBackNode = &curNode
			fmt.Println(wayBackNode)
			for wayBackNode.parent != nil {
				fmt.Println(wayBackNode.parent)
				wayBackNode = wayBackNode.parent
			}
			break MyLoop
		}

		neighbors := getNeighbors(labyMap, curNode, passNodes)
		for i := range(neighbors){
			queue.PushBack(neighbors[i])
		}

	}
}
